package com.andrewjb;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class type <code>HangmanGame</code>.
 * @author  Andrew Beattie
 */
public class HangmanGame {

    private static final String ANSWER = "cat";

    private Scanner scanner;
    private String latestGuess;
    private List<String> guessedLetters;
    private List<String> correctLetters;
    private List<String> hangmanStrikesAsciiLibrary;
    private int strikes;



    /**
     * Begin the <code>HangmanGame</code> program flow.
     *
     */
    public void startGame() {

        // Before doing anything, make sure all objects are instantiated that need to be
        scanner = new Scanner(System.in);
        guessedLetters = new ArrayList<>();
        correctLetters = new ArrayList<>();
        hangmanStrikesAsciiLibrary = new ArrayList<>();

        // Set up pre-game objects with necessary data
        setupCorrectLettersList();
        setupHangmanStrikesAscii();

        // Ask the player for their first guess
        getNewGuess();

    }



    /**
     * Get a new guess entry from the player.
     *
     */
    private void getNewGuess() {

        // Display the most current game state to the player before requesting they input a letter
        System.out.println("\n╸ --------------------------------------------------------------");
        System.out.println(hangmanStrikesAsciiLibrary.get(strikes));
        checkWinLoseCondition();
        printAllGuessedLetters();

        // Request the player input a letter
        System.out.println("\n◆ GUESS ONE LETTER: ");
        latestGuess = scanner.next();
        System.out.print("\n");

        // Check to see if that letter matches one from the correctLetters List
        if (letterWasCorrect()) {
            System.out.println("░ \"" + latestGuess.toUpperCase() + "\" is correct!\n");
            getNewGuess();

        // If the previous check fails, check that the letter doesn't match a letter from the guessedLetters List
        } else if (letterWasAlreadyGuessed()) {
            System.out.println("▌ \"" + latestGuess.toUpperCase() + "\" was already guessed.\n");
            getNewGuess();

        // If the previous two checks both fail, then the guess must be incorrect, and the player has earned a strike
        } else {
            System.out.println("█ \"" + latestGuess.toUpperCase() + "\" is incorrect, try again.\n");
            strikes++;
            getNewGuess();

        }

    }



    /**
     * Print every letter than has already been guessed to the console.
     *
     */
    private void printAllGuessedLetters() {

        // Only display the guessedLetters if the guessedLetters List isn't empty
        if (guessedLetters.size() > 0) {
            System.out.print("╸ Letters already guessed: ");

            // For each String object in the guessedLetters List, print that object
            for (String guess : guessedLetters) {
                System.out.print(guess.toUpperCase() + " ");

            }

            System.out.print("\n");
        }

    }



    /**
     * Return 'true' if the most recent letter that the player guessed matches one of the remaining correctLetters.
     *
     */
    private boolean letterWasCorrect() {

        // Step through the correctLetters List and compare each one to the player's latestGuess
        // correctLetters.size(); is used as the For Loop's termination condition to insures that
        // the entire List is checked without going beyond the number of letters contained in it.
        for (int index = 0; index < correctLetters.size(); index++) {

            String correctLetterFromIndex = correctLetters.get(index);

            // If a letter from correctLetters is equal to the player's latestGuess,
            // then it is removed from correctLetters, and added to guessedLetters.
            if (theseLettersAreEqual(latestGuess, correctLetterFromIndex)) {
                correctLetters.remove(index);
                guessedLetters.add(latestGuess);
                return true;

            }

        }

        // If the player's latestGuess doesn't match any of the letters in correctLetters, return 'false'
        return false;

    }



    /**
     * Return 'true' if the most recent letter that the player guessed matches one of the letters in guessedLetters.
     *
     */
    private boolean letterWasAlreadyGuessed() {

        // Step through each String object in guessedLetters and check it against the player's latestGuess
        for (String guess : guessedLetters) {

            // If the player's latestGuess matches one of the String objects contained within guessedLetters,
            // letterWasAlreadyGuessed() returns 'true'
            if (theseLettersAreEqual(guess, latestGuess)) {
                return true;

            }

        }

        // If the player's latestGuess doesn't match one of the String objects contained within guessedLetters,
        // add the latestGuess to the guessedLetters List,
        // letterWasAlreadyGuessed() then returns 'false'
        guessedLetters.add(latestGuess);
        return false;

    }



    /**
     * Return 'true' if the two incoming Strings are equal after they've been converted to lowercase.
     *
     * @param letterOne
     *        First String parameter that will be compared.
     *
     * @param letterTwo
     *        Second String parameter that will be compared.
     */
    private boolean theseLettersAreEqual(String letterOne, String letterTwo) {
        // This is done to insure that the player can use uppercase OR lowercase letters
        return letterOne.toLowerCase().equals(letterTwo.toLowerCase());
    }



    /**
     * Terminate the program if the player has met either the lose or the win condition.
     *
     */
    private void checkWinLoseCondition() {

        // The player wins the game if the correctLetters List is empty
        if (correctLetters.size() == 0) {
            System.out.println("\n◆ *** YOU WIN ***");
            System.exit(0);

        }

        // The player loses the game when strikes reaches 7
        if (strikes == 7) {
            System.out.println("\n█ *** YOU LOSE ***");
            System.exit(0);
        }

    }



    /**
     * Step through the ANSWER String and create a List that contains each letter for later use.
     *
     */
    private void setupCorrectLettersList() {

        for (int n = 0; n < ANSWER.length(); n++) {
            correctLetters.add(ANSWER.substring(n, n + 1));
        }

    }



    /**
     * Add 8 String objects to the hangmanStrikesAsciiLibrary List.
     *
     * Each index is associated with the number of strikes the player has.
     *
     */
    private void setupHangmanStrikesAscii() {

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|          \n" +
                        "|          \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|          \n" +
                        "|      \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|          \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|      \\  \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|     |\\  \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|          \n" +
                        "|    /|\\  \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|          \n" +
                        "|     O    \n" +
                        "|    /|\\  \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

        hangmanStrikesAsciiLibrary.add(
                " _____     \n" +
                        "|     |    \n" +
                        "|     O    \n" +
                        "|    /|\\  \n" +
                        "|    / \\  \n" +
                        "|_________ \n" +
                        "|         |\n");

    }


}
